# Moi c'est David Gond, ravi de vous rencontrer !

Je suis un étudiant français actuellement à l'Université du Québec à Chicoutimi (UQAC) dans le programme
de Maîtrise en Informatique avec majeure Jeux Vidéo. J'effectue mes études dans le cadre d'un double-diplôme avec 
l'ENSEIRB-MATMECA qui est mon école d'origine, à Bordeaux.

J'ai commencé la programmation en 2011, en classe de Troisième, sur calculatrice TI et j'y ai trouvé une réelle passion.

## 2014 - 2015 | Supinfo
Après l'obtention de mon Baccalauréat Scientifique spécialité Sciences de l'Ingénieur, j'ai suivi une année à l'école d'informatique privée Supinfo, à Toulouse, où j'ai
effectué des projets en équipe en C et Python, respectivement un 
*Conway's Game Of Life* ([page Wikipedia](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)) et un 
*Black Box* ([page Wikipedia](https://en.wikipedia.org/wiki/Black_Box_(game))) qui m'ont donné goût à la création de
jeux vidéo.

## 2015-2016 | Projet personnel
L'année suivante fut l'opportunité d'apprendre le C++ en autodidacte lors de la réalisation d'un projet personnel de jeu RPG avec combats
au tour par tour. Après avoir tenté *SDL* et *SFML*, j'ai essayé *Qt* afin de profiter des widgets déjà implémentés.
J'ai pu réaliser un petit jeu de combat au tour par tour où trois personnages affrontent trois ennemis. Ce jeu est très léger
car je n'étais alors qu'un débutant, tant en programmation qu'en conception et il souffrait de nombreux défauts. Il m'a
toutefois apporté beaucoup de satisfaction et de confiance dans mon apprentissage des langages de programmation.

## 2016 - 2018 | IUT
Après une année sur ce projet personnel, j'ai intégré l'IUT de Blagnac, proche de Toulouse. La très grande majorité de mes
compétences en programmation ainsi qu'en communication provient des deux ans passés dans cet établissement.
J'y ai travaillé sur de nombreux projets en C, Java ainsi qu'en SQL et PHP qui ont diversifié mes compétences techniques
tout en améliorant ma manière de travailler en équipe. J'ai participé à deux sessions de La Nuit de L'info, un hackathon
organisé entre plusieurs écoles de France pendant vingt-quatre heures où j'ai entre autres réalisé un bot Twitter qui réagit
à des messages publics en proposant une conversation en message privé, dans le but de rediriger des demandes d'aide.

## 2018 - 2019 | RGU
Suite à l'obtention de mon DUT, j'ai profité de l'opportunité de partir étudier à l'étranger dans le cadre d'un DUETI qui
me permettait d'ajouter un bachelor à mes diplômes. J'ai donc étudié en Écosse pendant un an, à la Robert Gordon University,
dans le programme *Computer Sciences, Digital Media* où j'ai appris à faire de la modélisation 3D, du montage audio/image/vidéo
mais également un site web en full-stack Javascript. J'ai pu découvrir le moteur de jeux *Unity* dans le cadre d'un cours
de réalisation de jeu, menant à la création d'une reprise du tout premier jeu *The Legend of Zelda* - une infime partie du jeu, cela dit.

## 2019 - 2021 | ENSEIRB-MATMECA
De retour en France, j'ai rejoint l'ENSEIRB-MATMECA qui m'a permis de grandement affiner ma manière de travailler lors des
projets réalisés pendant les deux premières années. J'ai pu participer à plusieurs GameJam, la plus importante étant la GameJam
EMMI organisée par l'ENSEIRB-MATMECA et l'IUT MMI de Bordeaux - et supportée par Ubisoft Bordeaux et Asobo - où j'ai réalisé avec mon équipe le jeu *The Electromancer*.
L'année suivante, j'ai été élu Président de GCC, le club de création de jeux vidéo de l'ENSEIRB-MATMECA qui dispense des
formations liées au jeu vidéo comme les moteurs de jeu ou la modélisation 3D, où j'ai pu organiser
avec l'exécutif du club la nouvelle session de la GameJam EMMI - à distance suite aux restrictions dues à la pandémie de COVID-19 - 
mais aussi dispensé une formation au moteur de jeux Godot.

## 2021 - présent | UQAC
Enfin, me voici aujourd'hui en troisième année dans un double-diplôme d'Ingénieur en Informatique et Maîtrise en Informatique
avec majeure Jeux Vidéo, plus motivé que jamais à entrer dans l'industrie du jeu vidéo pour fournir une expérience de qualité
aux joueurs et joueuses du monde entier.

<br/>

Merci d'avoir lu ce roman sur mes études et j'espère que mon histoire vous a intéressée. N'hésitez pas à vous rendre sur
[la page de mes projets]({{< ref "/showcase" >}} "Mes projets") pour découvrir certaines de mes réalisations.

Si vous souhaitez me contacter, les réseaux sur la [page principale]({{< ref "/" >}} "Accueil") devraient vous donner 
toutes les informations nécessaires.
