+++
tags = ['Videogame', 'Unity', 'Multiplayer', '2D']
date = "2022-04-30"
description = "Retrieve the Queen's jewels to save the Kingdom!"
slug = "pour-les-yeux-de-la-reine"
title = "Pour les Yeux de la Reine"
cover = "cover.png"
UseRelativeCover = true
coverColor = "seashell"
link = "https://octojul.itch.io/pyr"
linkTitle = "Itch.io"
+++

## My role in the project

I was in charge of:
- Git settings, as well as GitLab and the IDE used by the team members.
- implementing the skills and weapons manager, the inventory, dynamically associate a key with a skill, factorize all weapons
into a base architecture.
- conceiving the snow map with narrow spaces and multiple paths.
- implementing the network sync of weapons and their skills' cast.
- implementing the skill bar and its display effects (cooldown, skill ready or lack of ammo).
- creating and handling the Scrum board used throughout the entire project, including the automation to ease and quicken
its use.

## The knowledge I acquired

The biggest challenge I had to deal with was to conceive the association between a key and a skill, because each weapon
has a set of skills placed in specific slots depending on the weapon type and weapons can be changed during the game.
My first version was not consistent enough to easily add each weapon, so I entirely reworked the inventory and the
weapon system focusing on the ease of implementing a weapon. In that way, I was able to integrate the
skills and weapons in the network so that they are synchronized between each client.
