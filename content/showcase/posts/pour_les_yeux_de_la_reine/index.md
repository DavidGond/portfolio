+++
tags = ['Videogame', 'Unity', 'Multiplayer', '2D']
date = "2022-04-30"
description = "Retrouvez les joyaux de la Reine pour sauver le Royaume !"
slug = "pour-les-yeux-de-la-reine"
title = "Pour les Yeux de la Reine"
cover = "cover.png"
UseRelativeCover = true
coverColor = "seashell"
link = "https://octojul.itch.io/pyr"
linkTitle = "Itch.io"
+++
## Mon rôle dans le projet

- J'ai configuré les paramètres de Git, GitHub et de l'IDE utilisé par tous les membres de l'équipe.
- J'ai conçu et développé le gestionnaire des compétences et des armes : inventaire, association dynamique touche-compétence, 
factorisation de toutes les armes en une architecture de référence.
- J'ai conçu la carte enneigée en ciblant des couloirs relativement étroits et plusieurs chemins possibles.
- J'ai mis en place la communication en réseau des armes et l'activation de leurs compétences.
- J'ai créé la barre des compétences et ses différentes fonctionnalités (temps de rechargement, compétence activable 
ou manque de munition).
- J'ai mis en place le tableau Scrum utilisé tout au long du projet, incluant l'automatisation de certaines manipulations
afin de rendre son utilisation la plus simple et rapide possible.

## Ce que j'ai acquis

Mon plus grand défi lors de ce projet a été de mettre en place l'association entre une touche et une compétence, sachant
que chaque arme a ses propres compétences, qui sont placées dans des slots différents et que l'on peut changer d'arme plusieurs
fois au cours d'une partie. 

Ma première version n'étant pas assez homogène pour nous permettre de facilement ajouter chaque arme du jeu, 
j'ai retravaillé entièrement le système d'inventaire et des armes en ciblant autant que possible la rapidité
d'implémentation des armes. Ce faisant, j'ai pu intégrer le fonctionnement des compétences et des armes en réseau 
de manière que tous les joueurs puissent voir les armes des autres et l'activation de leurs compétences.
