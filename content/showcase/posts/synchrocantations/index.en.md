+++
tags = ['Videogame', 'Unity', 'Multiplayer', '2D', 'GameJam']
date = "2022-02-27"
description = "Protect your castle in a rhythm struggle in coop!"
slug = "synchrocantations"
title = "Synchrocantations"
cover = "cover.png"
UseRelativeCover = true
link = "https://aymeric4444.itch.io/syncrocantation"
linkTitle = "Itch.io"
+++

## My role in the project

I was in charge of:
- keyboard and controllers input using Unity Input System.
- implementing the falling notes and the press precision. 

## The knowledge I acquired

The biggest challenge I faced during this GameJam was to allow players to play with the keyboard **and** the controllers.
When a controller was connected, the game was supposed to be able to use it while still recognizing the keyboard,
to allow playing with either only the keyboard, the keyboard and a controller, or two controllers.
I had troubles to make this behaviour through the Input System, but I learned more about how it works. This helped me to
implement my own input system on another project ([link here]({{< ref "/showcase/posts/systeme_sol_rv" >}})).

I also learned how to handle the button press precision as seen in other rhythm games. The precision formula accepted a
variable speed so that when coupled with a variable note density it allowed changes in the difficulty depending on the 
elapsed time.
