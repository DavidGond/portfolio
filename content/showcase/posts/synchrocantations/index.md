+++
tags = ['Videogame', 'Unity', 'Multiplayer', '2D', 'GameJam']
date = "2022-02-27"
description = "Protégez votre château dans une lutte en rythme en duo !"
slug = "synchrocantations"
title = "Synchrocantations"
cover = "cover.png"
UseRelativeCover = true
link = "https://aymeric4444.itch.io/syncrocantation"
linkTitle = "Itch.io"
+++

## Mon rôle dans le projet

- J'ai utilisé Unity Input System pour gérer les entrées du clavier et des manettes.
- J'ai implémenté le défilement des notes et la barre de précision des appuis.

## Ce que j'ai acquis

Mon plus grand défi lors de cette GameJam a été de permettre aux joueurs de jouer avec le clavier et les manettes.
En connectant une manette, le jeu devait être capable de l'utiliser tout en continuant de prendre en compte le clavier,
pour permettre de jouer avec le clavier uniquement, le clavier et une manette, ou encore avec deux manettes. 
J'ai eu des difficultés à permettre ce comportement avec l'Input System, mais j'ai ainsi pu en apprendre plus sur son 
fonctionnement. Cela m'a aidé à implémenter mon propre système de saisie dans un autre projet
([lien]({{< ref "/showcase/posts/systeme_sol_rv" >}})).

J'ai également appris à reproduire la gestion de la précision de l'appui d'un bouton par rapport aux notes affichées
à l'écran, comme dans de nombreux jeux de rythmes existants. Le calcul de la précision permettait d'avoir
une vitesse de défilement variable. Couplé à la possibilité d'augmenter la densité de notes, il nous était possible de
modifier dynamiquement la difficulté du jeu en fonction du temps passé.
