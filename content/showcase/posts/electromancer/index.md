+++
tags = ['Videogame', 'Unity', 'Multiplayer', '2D', 'GameJam']
date = "2020-01-28"
description = "Infiltrez-vous en coopération dans un monde cyberpunk pour sauver votre fils !"
slug = "electromancer"
title = "The Electromancer"
cover = "cover.png"
UseRelativeCover = true
link = "https://metamaus.itch.io/the-electromancer"
linkTitle = "Itch.io"
+++

## Mon rôle dans le projet

J'ai développé la très grande partie du code du jeu, qui consiste principalement en :

- le déplacement des personnages et leur compétence individuelle (tir et piratage)
- la barre de séparation de l'écran, son apparition/disparition en fonction de la distance entre les personnages

## Ce que j'ai acquis

Ce jeu de GameJam a été le premier jeu que j'ai réalisé en équipe et le second sur Unity. J'y ai donc appris beaucoup 
de choses sur le fonctionnement de Unity et ses possibilités en 2D. 
