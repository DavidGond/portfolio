+++
tags = ['Videogame', 'Unity', 'Multiplayer', '2D', 'GameJam']
date = "2020-01-28"
description = "Cooperate to infiltrate a corp in a cyberpunk world to save your son!"
slug = "electromancer"
title = "The Electromancer"
cover = "cover.png"
UseRelativeCover = true
link = "https://metamaus.itch.io/the-electromancer"
linkTitle = "Itch.io"
+++

## My role in the project

I wrote almost all the code used by the game, which consists in:
- moving the characters and use their abilities (shoot and hacking).
- handling the split-screen line, toggling it depending on the distance between the characters. 

## The knowledge I acquired

This GameJam game was the first I made in a team and the second using Unity. I have learned many things about how
Unity works and its functionalities in 2D.
