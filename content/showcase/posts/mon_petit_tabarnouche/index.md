+++
tags = ['Videogame', 'Unity', 'Solo', '3D']
date = "2022-06-20"
description = "Occupez-vous d'une boule de poils pensante !"
slug = "mon_petit_tabarnouche"
title = "Mon petit Tabarnouche"
cover = "cover.png"
UseRelativeCover = true
coverColor = "peru"
link = "https://aymeric4444.itch.io/mon-petit-tabarnouche"
linkTitle = "Itch.io"
+++

## Mon rôle dans le projet

- J'ai commencé par mettre en place l'interface utilisateur, les boutons et les jauges ainsi que leurs interactions avec la souris.
- J'ai ensuite travaillé sur l'enchaînement des dialogues et les interactions entre les jauges et le système de dialogue.
- Enfin, je me suis occupé du déplacement du personnage, en créant un système de piste menant à une position cible. 
Ce système peut être appelé plusieurs fois pour définir un chemin que le personnage parcourt par lui-même.

## Ce que j'ai acquis

Lors de l'intégration des dialogues et des actions du joueur, j'ai pu apprendre comment synchroniser 
les animations et les appels au code, mais aussi comment interrompre une animation pour ensuite la reprendre ou l'annuler.
