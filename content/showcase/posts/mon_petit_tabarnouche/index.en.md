+++
tags = ['Videogame', 'Unity', 'Solo', '3D']
date = "2022-06-20"
description = "Take care of a thinking hairball (not literal)!"
slug = "mon_petit_tabarnouche"
title = "Mon petit Tabarnouche"
cover = "cover.png"
UseRelativeCover = true
coverColor = "peru"
link = "https://aymeric4444.itch.io/mon-petit-tabarnouche"
linkTitle = "Itch.io"
+++

## My role in the project

I was in charge of:
- prototyping the user interface, its buttons and their interactions with the mouse.
- helping in the implementation of the dialogue system and its integration with the gauges.
- creating a trail system to move the Tabarnouche to a target position. This system can be called multiple times to
define a path followed by the character.

## The knowledge I acquired

I learned how to sync animations with code calls and how to pause an animation that could then be resumed or canceled.
