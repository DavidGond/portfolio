+++
tags = ['Videogame', 'Unity', 'Multiplayer', '3D']
date = "2021-12-22"
description = "Résolvez le meurtre d'une célébrité façon Cluedo !"
slug = "troubles-manigances"
title = "Troubles et manigances à Little Mist Eire"
cover = "cover.png"
UseRelativeCover = true
coverColor = "skyblue"
link = "https://aymeric4444.itch.io/troubles-et-manigances-little-mist-eire"
linkTitle = "Itch.io"
+++

## Mon rôle dans le projet

- J'ai configuré les paramètres de Git, GitLab et de l'IDE utilisé par tous les membres de l'équipe.
- J'ai mis en place la gestion des compétences du troubleur (activation et rechargement).
- J'ai réalisé l'interface utilisateur des parties enquête et vote.
- J'ai doublé le narrateur dans la cinématique d'introduction.

## Ce que j'ai acquis

Ce projet étant le tout premier en réseau que j'ai réalisé, j'ai beaucoup appris sur les appels RPCs
à travers l'utilisation de Photon. J'y ai vu les défis que pose le jeu en réseau en comparaison aux jeux purement locaux
et cela m'a poussé à revoir ma manière de développer afin de limiter les potentiels problèmes de synchronisation entre
les différents clients. J'ai eu l'occasion de réfléchir sur l'architecture serveur-client en définissant le serveur comme
autorité principale sur de multiples éléments du jeu dans le but de réduire les possibilités de triche, problème qui 
apparaît bien moins fréquemment dans les jeux locaux.

C'est aussi ce projet qui m'a beaucoup appris sur la réalisation de l'interface utilisateur, en utilisant les différentes
ancres pour placer les éléments et faire en sorte que l'affichage reste fonctionnel malgré les variations de taille
d'écran.

J'ai également découvert quelque chose qui a changé ma manière de concevoir un jeu : les délégués. En utilisant les délégués,
nous pouvons traiter beaucoup d'actions de la même façon que les interactions du joueur, qui utilise beaucoup le patron
de conception Observeur. J'aime beaucoup ce patron puisqu'il rend l'intégration relativement simple.

Enfin, c'est le premier projet de jeu que j'ai réalisé avec autant de personnes (6) et en Scrum. Cela a été l'occasion
de tester différentes manières de communiquer au sein de l'équipe, à travers le tableau des sprints, des salons de discussion
ainsi qu'avec les outils de GitLab.
