+++
tags = ['Videogame', 'Unity', 'Multiplayer', '3D']
date = "2021-12-22"
description = "Solve the mystery of a famous person's death in the Clue way!"
slug = "troubles-manigances"
title = "Troubles et manigances à Little Mist Eire"
cover = "cover.png"
UseRelativeCover = true
coverColor = "skyblue"
link = "https://aymeric4444.itch.io/troubles-et-manigances-little-mist-eire"
linkTitle = "Itch.io"
+++

## My role in the project

I was in charge of:
- Git settings, as well as GitLab and the IDE used by the team members.
- the troublemaker's skills (cast and cooldown).
- the UI in the investigation and vote parts.
- voice acting the narrator in the trailer.

## The knowledge I acquired

Being the very first network video game I made, there were many things to learn, such as RPCs to call remote code in Photon.
Making a networked game is harder than making an offline game and I had to adapt to this difference in 
order to make sure that each client stays synchronized with the others. It offered me the chance to work on the
server-client architecture by defining the server as main authority over many aspects of the game, reducing potential 
cheating possibilities, which is less frequent in offline games.

Thanks to this project I learned how to make the UI by using anchors to place elements and make sure that it stays
functional despite changes in the screen size.

I also discovered something that completely changed the way I conceive games, which are delegates. By using delegates we
can treat many actions as events just like interactions from the player, which makes a heavy use of the Observer design
pattern. I really like this design pattern as it makes integration relatively simple. 

Last but not least, this was the first video game project where I participated within a team of 6 people and following
the Scrum framework. I was able to try different ways of communication within the team, using the sprint board, discussion
channels, and GitLab tools.
