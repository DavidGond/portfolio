+++
tags = ['Videogame', 'Unity', 'Solo', '3D']
date = "2022-04-30"
description = "The discovery of planets in virtual reality!"
slug = "systeme-sol-rv"
title = "Système SOL-RV"
cover = "cover.png"
UseRelativeCover = true
link = "https://aymeric4444.itch.io/systeme-sol-rv"
linkTitle = "Itch.io"
+++

## My role in the project

I was in charge of:
- implementing the platform controls (translation and rotation) and virtual hands movements (animations, collisions with
the screen UI).
- implementing the controllers' handler (button press and hold, orientation) from their raw data.

## The knowledge I acquired

Being my very first VR project, I learned how to use OpenXR in a Unity project and how to gather the controllers' position
data. Because this data were raw and real time, I implemented a buttons' state buffering to compare with the current state
to be able to know if a button were pressed or hold. Adding a behaviour when a button was pressed was then an easy task.

One of the new things I learned was to animate de virtual hands, and especially their fingers depending on which buttons
were pressed. The animation's goal was to improve the player's immersion.
