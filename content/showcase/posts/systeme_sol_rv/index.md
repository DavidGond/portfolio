+++
tags = ['Videogame', 'Unity', 'Solo', '3D']
date = "2022-04-30"
description = "À la découverte des planètes en réalité virtuelle !"
slug = "systeme-sol-rv"
title = "Système SOL-RV"
cover = "cover.png"
UseRelativeCover = true
link = "https://aymeric4444.itch.io/systeme-sol-rv"
linkTitle = "Itch.io"
+++

## Mon rôle dans le projet

- J'ai implémenté les contrôles de la plateforme (translations et rotations) et les déplacements des mains virtuelles 
(animations, collisions avec l'interface de l'écran de la plateforme).
- J'ai implémenté la gestion des contrôleurs (appui simple et long sur les boutons, orientation) à partir des données 
brutes des contrôleurs.

## Ce que j'ai acquis

Ce projet étant mon tout premier projet en RV, j'ai appris comment utiliser OpenXR dans un projet Unity et comment récupérer
les données de positionnement envoyées par les contrôleurs. Puisque ces données étaient brutes et en temps réel, j'ai
implémenté une mise en mémoire de l'état des boutons pour les comparer à l'instant actuel et ainsi savoir quel bouton est
pressé d'un appui simple ou prolongé. Il était ainsi facile d'ajouter un comportement lors de l'appui des boutons pour 
déplacer la plateforme.

J'ai également appris à animer les doigts des mains virtuelles en fonction des boutons pressés pour rendre plus immersives
les interactions du joueur.
